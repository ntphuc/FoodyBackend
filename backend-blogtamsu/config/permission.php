<?php
return  [
    'ViewArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'CreateArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'

    ] ,
    'EditArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'ActiveArticle' =>[
        'Admin' ,
        'Editor'
    ] ,
    'DeleteArticle' => [
        'Admin' ,
        'Editor' ,
        'Normal'
    ] ,
    'PublishArticle' => [
        'Admin' ,
        'Editor'
    ],
    'ReadUser' => [
        'Admin'
    ] ,
    'ActiveUser' => [
        'Admin'
    ] ,
    'EditUser' => [
        'Admin'
    ] ,
    'DeleteUser' => [
        'Admin'
    ] ,
    'CreateUser' => [
        'Admin'
    ]
] ;