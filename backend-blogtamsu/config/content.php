<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 2/29/2016
 * Time: 1:44 PM
 */
return [
    'category' => [
        [
            'name' => 'Parent Cat',
            'url' => 'parent-cat',
            'child' => [
                ['name' => 'Child 1', 'url' => 'child-1'],
                ['name' => 'Child 2', 'url' => 'child-2'],
            ]
        ],
        [
            'name' => 'Parent Cat 2',
            'url' => 'parent-cat-2',
            'child' => [
                ['name' => 'Child 3', 'url' => 'child-3'],
                ['name' => 'Child 4', 'url' => 'child-4'],
            ]
        ],
    ],
];