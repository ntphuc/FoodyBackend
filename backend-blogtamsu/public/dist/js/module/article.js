$('#article_form').submit(function (event) {
    event.preventDefault();
    $categories = [] ;
    $primary_category =[] ;
    $array = $('input[name="category[]"]:checked').each(function()
    {
        if($(this).is(':checked'))
            $categories.push($(this).val());
    });

    $primary_category.push({
        url : $('#inputPrimaryCateUrl').val()   ,
        category_name :$('#inputPrimaryCateName').val()   ,
    });

    var formData = {
        _token : $("input[name='_token']").val()  ,

        name: $("#name_article").val() === '' ? swal({
            title: AdminCPLang.lang_20,
            type: 'error'
        }) : $("#name_article").val(),
        lang: $("#language").val() === '' ? swal({
            title: AdminCPLang.lang_20,
            type: 'error'
        }) : $("#language").val(),
        description: $("#description_article").val() === ''? swal({
            title: AdminCPLang.lang_25,
            type: 'error'
        }) : $("#description_article").val(),
        content: $("#editor").val() === '' ? swal({
            title: AdminCPLang.lang_21,
            type: 'error'
        }) : $("#editor").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: AdminCPLang.lang_23,
            type: 'error'
        }) : $("input[name='tags']").val(),
        category: $categories.length === 0  ? swal({
            title: AdminCPLang.lang_22,
            type: 'error'
        }) : JSON.stringify($categories),
        thumbnail: $("#id_of_the_target_input").val() === '' ? swal({
            title: AdminCPLang.lang_24,
            type: 'error'
        }) : $("#id_of_the_target_input").val(),
        primary_category :$('#inputPrimaryCateUrl').val() ===''  ? swal({
            title: AdminCPLang.lang_26,
            type: 'error'
        }) : JSON.stringify($primary_category),
    } ;
    if (!formData.name || !formData.primary_category || !formData.content || !formData.thumbnail || !formData.description || $array.length === 0   ) {
        return;
    }

    $.ajax({
            type: "POST",
            url: '/media/article/create-post/',
            dataType: 'json',
            data: formData,
            success: function (res) {
                event.preventDefault();
                    swal({title: res.msg, type: res.status} , function(isConfirm){
                        if(isConfirm){
                            location.reload();
                        }
                    });
            }
        }
    );
});

$('#article_form_edit').submit(function (event) {
    event.preventDefault();
    $categories = [] ;
    $primary_category =[] ;
    $array = $('input[name="category[]"]:checked').each(function()
    {
        if($(this).is(':checked'))
            $categories.push($(this).val());
    });

    $primary_category.push({
        url : $('#inputPrimaryCateUrl').val()   ,
        category_name :$('#inputPrimaryCateName').val()   ,
    });


    var formData = {
        _token : $("input[name='_token']").val()  ,

        article_id: $('input[name=article_id]').val() === '' ? null : $('input[name=article_id]').val(),
        name: $("#name_article").val() === '' ? swal({
            title: AdminCPLang.lang_20,
            type: 'error'
        }) : $("#name_article").val(),
        lang: $("#language").val() === '' ? swal({
            title: AdminCPLang.lang_20,
            type: 'error'
        }) : $("#language").val(),
        description: $("#description_article").val() === ''? swal({
            title: AdminCPLang.lang_25,
            type: 'error'
        }) : $("#description_article").val(),
        content: $("#editor").val() === '' ? swal({
            title: AdminCPLang.lang_21,
            type: 'error'
        }) : $("#editor").val(),
        tags: $("input[name='tags']").val() === '' ? swal({
            title: AdminCPLang.lang_23,
            type: 'error'
        }) : $("input[name='tags']").val(),
        category: $categories.length === 0  ? swal({
            title: AdminCPLang.lang_22,
            type: 'error'
        }) : JSON.stringify($categories),
        thumbnail: $("#id_of_the_target_input").val() === '' ? swal({
            title: AdminCPLang.lang_24,
            type: 'error'
        }) : $("#id_of_the_target_input").val(),
        primary_category :$('#inputPrimaryCateUrl').val() ===''  ? swal({
            title: AdminCPLang.lang_26,
            type: 'error'
        }) : JSON.stringify($primary_category),
    } ;
    if (!formData.name || !formData.primary_category || !formData.content || !formData.thumbnail || !formData.description || $array.length === 0   ) {
        return;
    }

    $.ajax({
            type: "POST",
            url: '/media/article/edit-post/',
            dataType: 'json',
            data: formData,
            success: function (res) {
                event.preventDefault();
                swal({title: res.msg, type: res.status} , function(isConfirm){
                    if(isConfirm){
                        location.reload();
                    }
                });
            }
        }
    );
});




function deleteArticle(target){
    var article_id = $(target).data('article_id');
    return swal({
        title: AdminCPLang.lang_1,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: AdminCPLang.lang_3,
        closeOnConfirm: false
    }, function () {
        $.ajax({
                type: "POST",
                url: '/media/article/delete',
                dataType: 'json',
                data: {id: article_id, select_action : 'Delete', type : 'ajax'}, // serializes the form's elements.
                success: function (res) {
                    swal({title: res.msg, type: res.status});
                    $('tr.post-item.post-id-' + article_id).remove();
                },
                error: function(resp) {
                    alert('Erorr!');
                }
            }
        );
    });
}

// review Article
function reviewArticle(target){
    var article_id = $(target).data('article_id');
    loadModalContent('reviewArticleModalBody', '/media/article/review/' + article_id)
    $("#reviewArticleModal").modal('show');
}

// mo cua so chon list to web publish
$("#btn-status").on('click', function(){
    var status = $(this).attr('status');
    if(status == "off"){
        $(".modal-body #webPublish ").slideDown(300);
        $(".modal-body #viewReviewArticle ").slideUp(300);
        $(this).html('Back');
        $(this).attr('status', 'on');
    }
    if(status == "on"){
        $(".modal-body #webPublish ").slideUp(300);
        $(".modal-body #viewReviewArticle ").slideDown(300);
        $(this).html('Publish');
        $(this).attr('status', 'off');
    }
});
$('.modal-content .modal-header #close').click(function(){
    // click vao nut dong cua so thi dong bang publish
    $(".modal-body #webPublish ").slideDown(300);
    $(".modal-body #viewReviewArticle ").slideUp(300);
    $('.modal-content .modal-header #btn-status').html('Publish');
    $('.modal-content .modal-header #btn-status').attr('status', 'off');
});
$("html").click(function (e) {                          // click ra ngoai dong cua so thi dong bang publish
    if ($('.modal-content').is(":visible")){
    }else {
        $('.modal-body #webPublish').slideDown(300);
        $('.modal-body #viewReviewArticle').slideUp(300);
        $('.modal-content .modal-header #btn-status').html('Publish');
        $('.modal-content .modal-header #btn-status').attr('status', 'off');
    }
});

// submit to list web publish
function summitToWebPublish(target){
    var article_id = $(target).data('article_id');
    var type_to_publish = $(target).data('type');
    var list = [] ;
    $array = $('input[name="listWeb[]"]:checked').each(function()
    {
        if($(this).is(':checked'))
            list.push($(this).val());
    });
    if(list == ''){
        alert("Error ! Choose Web Fail !");
    }else{
        $.ajax({
                type: "POST",
                url: '/media/article/submit',
                dataType: 'json',
                data: {id: article_id , st: type_to_publish, li: list},
                success: function (res) {
                    if(res.status == "error"){
                        swal({title: res.msg, type: res.status});
                    }else{
                        $('#reviewArticleModal').modal('hide');
                        swal({title: res.msg, type: res.status});
                        location.reload();
                    }
                },
                error: function(resp) {
                    alert('Erorr!');
                }
            }
        );
    }

}

// Active status Article
function activeArticle(target){
    var article_id = $(target).data('article_id');
    var st = $(target).data('status');
    $.ajax({
            type: "POST",
            url: '/media/article/update-status',
            dataType: 'json',
            data: {id: article_id , st: st}, // serializes the form's elements.
            success: function (res) {
                $('#reviewArticleModal').modal('hide');
                swal({title: res.msg, type: res.status});
                setTimeout(function(){
                    window.location.reload(1);
                }, 2000);
            },
            error: function(resp) {
                alert('Erorr!');
            }
        }
    );
}
