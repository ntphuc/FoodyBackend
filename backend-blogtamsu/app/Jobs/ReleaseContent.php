<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Events\ProcQueue  ;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReleaseContent extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $_apiRequest ;
    private $data  ; 
    public function __construct($_apiRequest , $data )
    {
        $this->_apiRequest = $_apiRequest ;
        $this->data = $data ; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Event::fire(new ProcQueue('add' , $this->_apiRequest  , $this->data  )) ;
    }
}
