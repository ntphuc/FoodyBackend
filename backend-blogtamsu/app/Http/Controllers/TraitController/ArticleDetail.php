<?php
namespace App\Http\Controllers\TraitController;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Jobs\ActionContent;
use App\Jobs\ReleaseContent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Models\Article;
use App\Http\Requests;
use App\Models\Tags;
use App\Http\Controllers\Controller;
use Gate ;
use Illuminate\Support\Facades\Input;

trait ArticleDetail
{

    public function getCreate()
    {
        $this->authorize('CreateArticle') ;
        $data['category'] = Category::all() ;
        return view('childs.article.create')->with($data);
    }

    function postCreate()
    {
        $this->authorize('CreateArticle') ;
        try {
            // Get Data From Input
            $input = $this->_request->except('_token', 'tags', 'category', 'seo_title', 'seo_description', 'seo_meta', 'primary_category');

            //Build Tag & Insert Tag
            $tags = explode(',', $this->_request->get('tags'));
            foreach ($tags as $item) {

                // If Not Have Tag
                if (Tags::where('name', $item)->count() == 0) {
                    $new_tag = new Tags();
                    $new_tag->name = $item;
                    $new_tag->url = str_slug($item, '-');
                    $new_tag->save();
                }
                //Build Tag To Insert
                $array_tag[] = ['tag_name' => $item, 'slug_url' => str_slug($item, '-')];
            }

            if (substr($this->_request->get('thumbnail'), 0, 4) == 'http') {
                $file = uniqid() . '.jpg';
                file_put_contents('filemanager/userfiles/' . $file, file_get_contents($this->_request->get('thumbnail')));
                $input['thumbnail'] = '/filemanager/userfiles/' . $file;
            }

            //Build Category
            $categories = json_decode($this->_request->get('category'));
            foreach ($categories as $item) {
                $category_explode = explode('/', $item);
                $category_array[] = ['url' => $category_explode[0], 'category_name' => $category_explode[1]];

            }

            $pri_cate = json_decode($this->_request->get('primary_category'));

            //Building SEO
            $this->_request->get('seo_title') != '' ? $seo_title = $this->_request->get('seo_title') : $seo_title = $this->_request->get('name');
            $this->_request->get('seo_description') != '' ? $seo_desc = $this->_request->get('seo_description') : $seo_desc = $this->_request->get('description');
            $this->_request->get('seo_meta') != '' ? $seo_meta = $this->_request->get('seo_meta') : $seo_meta = $this->_request->get('name');

            //Create POST
            $article = new Article();
            foreach ($input as $key => $value) {
                $article->$key = $value;
            }
            //Save Model
            $article->save();
            //Return Value
            return json_encode(['status' => 'success', 'msg' => 'Post Successfully']);

        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }

    public function getEdit($article_id)
    {
        $this->authorize('EditArticle') ;
        $article = Article::find($article_id);
        $this->authorize('PostOfUser' , $article ) ;
        return view('childs.article.edit')->with(['article' => $article]);
    }

    public function anyEditPost(Request $request)
    {
        $this->authorize('EditArticle') ;
        try {
            // Get Data From Input
            $input = $this->_request->except('_token', 'tags', 'category', 'seo_title', 'seo_description', 'seo_meta', 'primary_category');

            //Build Tag & Insert Tag
            $tags = explode(',', $this->_request->get('tags'));
            foreach ($tags as $item) {

                // If Not Have Tag
                if (Tags::where('name', $item)->count() == 0) {
                    $new_tag = new Tags();
                    $new_tag->name = $item;
                    $new_tag->url = str_slug($item, '-');
                    $new_tag->save();
                }
                //Build Tag To Insert
                $array_tag[] = ['tag_name' => $item, 'slug_url' => str_slug($item, '-')];
            }

            if (substr($this->_request->get('thumbnail'), 0, 4) == 'http') {
                $file = uniqid() . '.jpg';
                file_put_contents('filemanager/userfiles/' . $file, file_get_contents($this->_request->get('thumbnail')));
                $input['thumbnail'] = '/filemanager/userfiles/' . $file;
            }

            //Build Category
            $categories = json_decode($this->_request->get('category'));
            foreach ($categories as $item) {
                $category_explode = explode('/', $item);
                $category_array[] = ['url' => $category_explode[0], 'category_name' => $category_explode[1]];

            }

            $pri_cate = json_decode($this->_request->get('primary_category'));

            //Building SEO
            $this->_request->get('seo_title') != '' ? $seo_title = $this->_request->get('seo_title') : $seo_title = $this->_request->get('name');
            $this->_request->get('seo_description') != '' ? $seo_desc = $this->_request->get('seo_description') : $seo_desc = $this->_request->get('description');
            $this->_request->get('seo_meta') != '' ? $seo_meta = $this->_request->get('seo_meta') : $seo_meta = $this->_request->get('name');

            //Edit POST
            $article = Article::find($this->_request->get('article_id'));
            foreach ($input as $key => $value) {
                $article->$key = $value;
            }

            //Extra Save Model
            $article->title = $this->_request->get('name');
            $article->slug_url = trim(str_slug($this->_request->get('name'), "-") . '-' . date('d-m-Y'));
            $article->seo_attr = ['title' => $seo_title, 'description' => $seo_desc, 'meta' => $seo_meta];
            $article->category = $category_array;
            $article->primary_category = $pri_cate;
            $article->tags = $array_tag;
            $article->lang = $this->_request->get('lang');


            //Save Model
            $article->save();


            //Return Value
            return json_encode(['status' => 'success', 'msg' => 'Post Successfully']);

        } catch (\Exception $e) {
            return json_encode(['status' => 'error', 'msg' => $e->getMessage()]);
        }


    }
}

?>