<?php
namespace App\Http\Controllers\TraitController;

use Illuminate\Http\Request;
use App\Jobs\ActionContent;
use App\Jobs\ReleaseContent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Models\Article;
use App\Http\Requests;
use App\Models\Tags;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

trait Tag
{

    function getTag()
    {
        $tag = $this->_request->get('term');
        $find_tag = Tags::where('name', 'like', "%$tag%")->take(4)->get();
        foreach ($find_tag as $items) {
            $array[] = $items->name;
        }
        return $array;
    }
}