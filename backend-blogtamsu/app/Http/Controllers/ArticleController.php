<?php

namespace App\Http\Controllers;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use App\Jobs\ActionContent;
use App\Jobs\ReleaseContent ;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Models\Article;
use App\Http\Requests;
use App\Models\Tags ;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\TraitController\ArticleDetail ;
use App\Http\Controllers\TraitController\Tag ;
class ArticleController extends Controller
{
    use ArticleDetail , Tag ;
    public $article_on_page = 15;

    public function __construct(Request $request )
    {
        $this->_request = $request ;
    }

    public function getIndex(Request $request)
    {
        $this->authorize('ViewArticle') ;

        $request->flash();
        $start_date = date('Y-m-t 00:00:00', time());
        $start_end = date('Y-m-t 23:59:59', time());


        $start_date = $request->has('start_date') ? $request->old('start_date') : $start_date;
        $end_date = $request->has('end_date') ? $request->old('end_date') : $start_end;

        $articles = Article::where('created_at', '>=',  new \DateTime($start_date))->where('created_at', '<=',  new \DateTime($end_date));

        if($request->has('key')){
            $keyword =  $request->old('key');
            $keyword = preg_replace('/\s\s+/', ' ', trim($keyword));
            $articles = $articles->where('title', 'like', '%'.$keyword.'%');
        }

        if($request->has('category')){
            $articles = $articles->where('category.url', $request->get('category') );
        }


        if($request->has('status')){
            $articles = $articles->where('status', $request->get('status') );
        }

        if($request->has('lang')){

            $articles = $articles->where('lang', $request->get('lang') );
        }

        if($request->has('creator')){
            $articles = $articles->where('creator.user_name', $request->get('creator') );
        }
        $request_all = $request->all();
        $articles = $articles->simplePaginate($this->article_on_page);

        return view('childs.article.index')->with('articles', $articles)->with('request_all' , $request_all);

    }
    public function getReview($id){
        $this->authorize('ViewArticle') ;
        $article = Article::findOrFail($id);
        return view('childs.article.review')->with(['article' => $article]);
    }

    public function postDelete(Request $request)
    {
        $this->authorize('DeleteArticle') ;
        $article = Article::find($request->get('id'));
        $this->authorize('PostOfUser' , $article ) ;

        $job = (new ActionContent($request->get('id'), '\App\Models\Article', 'delete', 'article'));
        $this->dispatch($job);
        $status = 'success';
        $msg = trans('article.delete_success');
        return json_encode(['status' => $status, 'msg' => $msg]);
    }

    public function postUpdateStatus(Request $request)
    {
        $this->authorize('ActiveArticle') ;
        $article_id = Article::findOrFail($request->get('id'));

        $status = 'error';
        $msg = trans('article.update_status_fail');
        try {
            $article_id->status = $request->get('st');
            if ($article_id->save()) {
                $status = 'success';
                $msg = trans('article.update_status_success');
            }
            return json_encode(['status' => $status, 'msg' => $msg]);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'error', 'msg' => $ex->getMessage()]);
        }
    }
}