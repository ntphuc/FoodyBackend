<?php
/**
 * Created by PhpStorm.
 * User: phucnt
 * Date: 18/12/2015
 * Time: 13:16
 */
namespace App\Repositories\Post ;
use App\Models\Post ;

class EloquentPost implements  PostRepository {
    function __construct()
    {
        $this->post = \App::make('Post') ;
    }

    function getAllPostPage($page, $user)
    {
        if (Post::paginate((int)$page)) {
        if ($user == 'all') {
            return Post::orderBy('created_at', 'DESC')->paginate((int)$page);
        } else {
            return Post::orderBy('created_at', 'DESC')->where('user_id', '=', $user)->paginate((int)$page);
        }
    }
    return false;
}

function getAllPost()
{
    $list_post = $this->post->all() ;
    if($list_post != null) {
        return $list_post;
    }
    return false;
}

function findPostbyId($id)
{
    $post = $this->post->find($id) ;
    if($post) {
        return $post;
    }
    return false;
}

function UpdatePost($id, $array)
{
    $post = $this->post->find($id) ;
    if($post != null) {
        foreach($array as $k => $v) {
            $post->$k = $v ;
        }
        if( $post->save() ){
            return true  ;
        }
    }
    return false;
}

public function DeletePost($id)
{
  $post = Post::find($id);
  if ($post) {
    $post->delete();
}
return false;
}

function getPostWhere( $where , $take , $order_by )
{
    $post = $this->post->whereRaw($where_array)->take($take)->orderByRaw($order_by)->get() ;
    return  $post  ;
}

function getPostPaginate($array, $paginate)
{
    return $post = $this->post->whereRaw($where_array)->orderByRaw($order_by)->paginate($paginate) ;
}

function InsertPost($array)
{
    $post = $this->post ;
    foreach($array as $k => $v) {
        $post->$k = $v ;
    }
    if( $post->save() ){
        return true  ;
    }else{
        return false ;
    }
}
function getPostbyCategory()
{
    return $this->post->with('category')->get() ;
}

public function getSearch($keyword, $page, $user)
{
    $keyword= preg_replace('/\s\s+/', ' ', trim($keyword));
    if ($user != 'all') {
        $post = Post::where('post_name', 'regexp', '/' . $keyword . '/i')
        ->where('user_id' , '=' , $user)
        ->orderBy('post_name', 'ASC')
        ->paginate((int)$page);
    } else {
        $post = Post::where('post_name', 'regexp', '/' . $keyword . '/i')
        ->orderBy('post_name', 'ASC')
        ->paginate((int)$page);
    }
    return $post;
}




public function ActivePost($id, $status)
{
 $post = Post::find($id);
 if ($post) {
    $post->post_status = $status;
    $post->save();
} else {
    return false;
}

}

public function getListPost($page, $user){
    if (Post::paginate((int)$page)) {
        if ($user == 'all') {
            return Post::orderBy('created_at', 'DESC')->paginate((int)$page);
        } else {
            return Post::orderBy('created_at', 'DESC')->where('user_id', '=', $user)->paginate((int)$page);
        }
    }
    return false;
}


}