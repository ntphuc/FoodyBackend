@extends('layouts.master')

@section('main_content')
    <section class="content-header">
        <h1>{{ trans('menu.list_articles') }}</h1>
        <a href="{{ URL::to('media/article/create') }}" class="btn btn-success">{{ trans('article.create_article') }}</a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{ trans('menu.media_zone') }}</li>
            <li class="active">{{ trans('menu.list_articles') }}</li>
        </ol>
    </section>
    <section class="content">
        <div>
            <div style="padding-bottom: 5px; margin-bottom: 5px; border-bottom: 1px solid #ddd">
                <form method="get" action="" autocomplete="off" role="form" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control search_top" name="key" id="key"
                               value="{{ old('key') }}"
                               autocomplete="off" placeholder="{{ trans('article.search_by_name') }}"
                               style="width: 150px;">
                    </div>
                    <div class="form-group">
                        <div id="reportrange" class="btn btn-default "
                             style="position: relative; display: inline-block">
                            <i class="glyphicon glyphicon-calendar"></i>
                            @if(old('start_date') != null && old('end_date') != null)
                                <span id="time_select">{{date(old('start_date'))}}
                                    - {{date(old('end_date')) }}</span>
                            @else
                                <span id="time_select">{{date('Y-m-01 00:00:00',time())}}
                                    - {{date('Y-m-t 23:59:59',time()) }}</span>
                            @endif
                            <b class="caret"></b>
                            <input type="hidden" name="start_date" id="start_date" value="{{ old('start_date') }}">
                            <input type="hidden" name="end_date" id="end_date" value="{{ old('end_date') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control"  name="category" data-live-search="true" data-width="120px">
                            <option value="" @if(!isset($request_all['category'])){{"selected"}}@endif >
                                {{ trans('article.category') }}
                            </option>
                            @foreach(config('content.category') as $key => $value)
                                <option
                                        @if(isset($request_all['category']) && $request_all['category'] == $value['url']){{"selected"}}@endif
                                        value="{{$value['url']}}">{{ucfirst($value['name'])}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="status" data-live-search="true" style="width:90px;">
                            <option @if(!isset($request_all['status'])){{"selected"}}@endif value="">{{ trans('article.status') }}</option>
                            <option @if(isset($request_all['status']) && $request_all['status'] == "draft"){{"selected"}}@endif value="draft">{{ trans('article.draft') }}</option>
                            <option @if(isset($request_all['status']) && $request_all['status'] == "published"){{"selected"}}@endif value="published">{{ trans('article.published') }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="lang" data-live-search="true" data-width="120px">
                            <option @if(!isset($request_all['lang'])){{"selected"}}@endif value="">{{ trans('article.lang') }}</option>
                            @foreach(config('admincp.lang_support') as $key => $value)
                                <option
                                        @if(isset($request_all['lang']) && $request_all['lang'] == $key){{"selected"}}@endif
                                        value="{{ $key }}">{{ ucfirst($value) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="creator" data-live-search="true" data-width="120px">
                            <option @if(!isset($request_all['creator'])){{"selected"}}@endif value="">{{ trans('article.creator') }}</option>
                            <?php
                            $creator = App\Models\User::all();
                            ?>
                            @foreach($creator as $value)
                                <option value="{{$value->user_name}}"
                                @if(isset($request_all['creator']) && $request_all['creator'] == $value->user_name){{"selected"}}@endif>{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" class="btn btn-danger" name="search" value="{{ trans('home.search') }}">
                </form>
            </div>
            <div class="post-container">
                <div class="box box-solid">
                    <div class="box-body no-padding">
                        <div>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>{{ trans('article.title') }}</th>
                                    <th>{{ trans('article.category') }}</th>
                                    <th>{{ trans('article.creator') }}</th>
                                    <th>{{ trans('article.approved_by') }}</th>
                                    <th>{{ trans('article.created_at') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $article)
                                    <tr id="" class="post-item post-id-{{$article->_id}}" >
                                        <td>
                                            <div style="width: 110px; float: left;">
                                                <img src="{{$article->thumbnail}}"
                                                     class="lazy_img image-responsive" width="110"
                                                     style="display: inline;">
                                            </div>
                                            <div style="margin-left: 120px;">
                                            <span class="title">
                                                <strong>{{$article->title}}</strong>
                                                    <div style="padding-top: 3px; color: #999; font-size: 11px;">{{ trans('article.by') }}
                                                        <strong>{{$article->creator['name']}}</strong> {{ trans('article.at') }} {{$article->created_at}}.
                                                    </div>
                                            </span>
                                                <div style="padding-top: 3px;">
                                                    <span class="label @if($article->status == 'draft'){{"label-primary"}} @else{{"label-danger"}} @endif " rel="source-status">{{ucfirst($article->status)}}</span>
                                                    <button class="btn btn-link btn-xs show-more-info" type="button"
                                                            data-toggle="collapse" data-target="#collapseExample{{$article->_id}}"
                                                            aria-expanded="false" aria-controls="collapseExample"><i
                                                                class="ion-more"></i></button>
                                                    <div class="collapse" id="collapseExample{{$article->_id}}">
                                                        <div class="well">
                                                            <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                                                                <tbody>
                                                                <tr>
                                                                    @foreach(config('admincp.lang_support') as $key => $value)
                                                                        @if($article->lang == $key)
                                                                            <td>{{ trans('article.lang') }} :</td>
                                                                            <td>{{ucfirst($value)}}</td>
                                                                        @endif
                                                                    @endforeach
                                                                </tr>
                                                                <tr>
                                                                    <td>{{ trans('article.category') }}:</td>
                                                                    <td>
                                                                        @foreach($article->category as $category)
                                                                            {{$category['category_name']}}{{" | "}}
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                @if(isset($article->seo_attr))
                                                                    <tr>
                                                                        <td>{{ trans('article.seo_title') }} :</td>
                                                                        <td>{{$article->seo_attr['title']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>{{ trans('article.seo_meta') }} :</td>
                                                                        <td>{{$article->seo_attr['meta']}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>{{ trans('article.seo_description') }} :</td>
                                                                        <td>{{$article->seo_attr['description']}}</td>
                                                                    </tr>
                                                                @endif
                                                                <tr>
                                                                    <td>{{ trans('article.web_published') }}:</td>
                                                                    <td>
                                                                        {{--@foreach($article->web_publish as $web)--}}
                                                                        {{--<a href="{{$web['url']}}">{{$web['name']}}</a>--}}
                                                                        {{--@endforeach--}}
                                                                        <a href="http://bongdaquocte.vn">bongdaquocte.vn | </a>
                                                                        <a href="http://vnexpress.net/">vnexpress.net</a>
                                                                    </td>
                                                                </tr></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-actions" style="visibility: visible;">
                                                    <span class="edit_article" id="edit_article"><a href="{{ URL::to('media/article/edit').'/'.$article->_id}}">{{ trans('article.edit') }}</a> | </span>
                                                    <span class="review_article"><a onclick="reviewArticle(this);" href="#review-article" data-article_id="{{$article->_id}}">{{ trans('article.review') }}</a> | </span>
                                                    <span class="delete_article"><a onclick="deleteArticle(this);" href="#delete-article" data-article_id="{{$article->_id}}">{{ trans('article.delete') }}</a>  </span>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="120">
                                            @foreach($article->primary_category as $category)
                                                <a href="{{ URL::to('media/article').'/'.$category['url']}}">
                                                    {{$category['category_name']}}</a>
                                            @endforeach

                                        </td>
                                        <td width="120">
                                            <a href="{{ URL::to('user').'/'.$article->creator['user_name']}}">
                                                {{$article->creator['name']}}</a>
                                        </td>
                                        <td width="120">
                                            @if(isset($article->approved_by))
                                                <a href="{{ URL::to('user').'/'.$article->approved_by['user_name']}}">
                                                    {{$article->approved_by['name']}}
                                                </a>
                                            @endif
                                        </td>
                                        <td width="140">
                                            <p>
                                                <span class="created_at">{{$article->created_at}}</span>
                                            </p>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div style="float: right;margin-right: 50px;">
                                {!! $articles->appends(Input::except('page'))->render() !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="reviewArticleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div style="width: 70%;" class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h2 class="col-md-9 modal-title" id="exampleModalLabel">{{ trans('article.review_article') }}</h2>
                    <button id="btn-status" style="width:130px;margin-left: 60px;" status="off"  id="summitToPublish" class="btn btn-success" type="button">
                        {{ trans('article.publish') }}
                    </button>
                </div>
                <div class="modal-body" id="reviewArticleModalBody">

                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_header')
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <link href="{{ asset('plugins/iCheck/minimal/blue.css') }}" rel="stylesheet">
@stop

@section('custom_footer')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('dist/js/module/article.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.js') }}"></script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#reportrange').daterangepicker(
                    {
                        ranges: {
                            '{{ trans('article.today') }}': [moment(), moment()],
                            '{{ trans('article.yesterday') }}': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            '{{ trans('article.last_7_day') }}': [moment().subtract(6, 'days'), moment()],
                            '{{ trans('article.last_30_day') }}': [moment().subtract(29, 'days'), moment()],
                            '{{ trans('article.this_month') }}': [moment().startOf('month'), moment().endOf('month')],
                            '{{ trans('article.last_month') }}': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        startDate: moment('{{ date('Y-m-01 00:00:00', time()) }}'),
                        endDate: moment('{{ date('Y-m-t 23:59:59', time()) }}')
                    },
                    function (start, end) {
                        $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
                        $('#start_date').val(start.format('YYYY-MM-DD HH:mm:ss'));
                        $('#end_date').val(end.format('YYYY-MM-DD HH:mm:ss'));
                    }
            );
            var timeSelect = moment('{{date('Y-m-01 00:00:00', time())}}').format('D MMMM, YYYY') + ' - ' + moment('{{date('Y-m-t 23:59:59', time())}}').format('D MMMM, YYYY');
            $('#time_select').html(timeSelect);
            $('#time_select2').html(timeSelect);
        });
    </script>
@stop